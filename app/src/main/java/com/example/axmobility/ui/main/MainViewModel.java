package com.example.axmobility.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Properties;

public class MainViewModel extends ViewModel {
    private final MutableLiveData<Properties> properties = new MutableLiveData<>();
    private final MutableLiveData<String> cpeid = new MutableLiveData<>();
    private final MutableLiveData<String> axVersion = new MutableLiveData<>();

    public LiveData<Properties> getProperties() { return properties; }
    public void setProperties(Properties props) { properties.postValue(props); }
    public LiveData<String> getCpeid() { return cpeid; }
    public void setCpeid(String id) { cpeid.postValue(id); }
    public LiveData<String> getAxVersion() { return axVersion; }
    public void setAxVersion(String version) { axVersion.postValue(version); }
}
