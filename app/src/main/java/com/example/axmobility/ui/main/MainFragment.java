package com.example.axmobility.ui.main;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.axiros.axmobility.android.utils.Constants;
import com.example.axmobility.R;
import com.example.axmobility.utils.CommonUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

public class MainFragment extends Fragment {
    public MainViewModel mViewModel;

    // Constructor *****************************************************************
    public static MainFragment newInstance() {
        return new MainFragment();
    }

    // Overrides *******************************************************************
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateUIInfo();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        mViewModel.getProperties().observe(getViewLifecycleOwner(), new Observer<Properties>() {
            @Override
            public void onChanged(Properties properties) {
                TextView textView;
                Set<String> keys = properties.stringPropertyNames();
                for (String key : keys) {
                    textView = requireView().findViewWithTag(key);
                    if (textView != null) {
                        if (key.equals(CommonUtils.KEY_DOWNLOAD_SPEED) || key.equals(CommonUtils.KEY_UPLOAD_SPEED)) {
                            textView.setText(String.format("%s Mbps", properties.getProperty(key)));
                        } else if (key.equals(CommonUtils.KEY_UDP_SPEED) || key.equals(CommonUtils.KEY_TCP_SPEED)) {
                            textView.setText(String.format("%s ms", properties.getProperty(key)));
                        } else {
                            textView.setText(properties.getProperty(key));
                        }
                    }
                }
            }
        });

        mViewModel.getCpeid().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String id) {
                TextView cpeTxtView = requireView().findViewById(R.id.cpeid);
                cpeTxtView.setText(id);
            }
        });

        mViewModel.getAxVersion().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String version) {
                TextView axVersion = requireView().findViewById(R.id.axmobilityVersion);
                axVersion.setText(version);
            }
        });

        getLastResults();
    }

    // Private Methods *************************************************************
    private void updateUIInfo() {
        PackageManager packageManager = requireActivity().getPackageManager();
        String packageName = requireActivity().getPackageName();

        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
            // Client version
            TextView versionName = requireView().findViewById(R.id.clientVersion);
            versionName.setText(packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(Constants.DEFAULT_LOG_TAG, "Client onCreateView", e);
        }
    }

    private void getLastResults() {
        String filename = requireContext().getFilesDir().getAbsolutePath() + "/" + CommonUtils.FILE_RESULTS;

        try {
            File configFile = new File(filename);
            if (configFile.exists()) {
                InputStream inputStream = new FileInputStream(configFile);
                Properties properties = new Properties();
                properties.load(inputStream);
                mViewModel.setProperties(properties);
            }
        } catch (Exception e) {
            Log.e(Constants.DEFAULT_LOG_TAG, "Client getLastResults", e);
        }
    }
}