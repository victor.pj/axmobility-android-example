package com.example.axmobility;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.axiros.axmobility.AxEvents;
import com.axiros.axmobility.android.AxMobility;
import com.axiros.axmobility.android.AxSettings;
import com.axiros.axmobility.android.utils.Constants;
import com.axiros.axmobility.events.TR143Events;
import com.axiros.axmobility.tr143.DownloadDiagnostic;
import com.axiros.axmobility.tr143.TCPDiagnostic;
import com.axiros.axmobility.tr143.TR143Diagnostic;
import com.axiros.axmobility.tr143.UDPDiagnostic;
import com.axiros.axmobility.tr143.UploadDiagnostic;
import com.example.axmobility.ui.main.MainFragment;
import com.example.axmobility.ui.main.MainViewModel;
import com.example.axmobility.utils.CommonUtils;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class MainActivity extends AppCompatActivity {
    private MainViewModel mainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                                       .replace(R.id.container, MainFragment.newInstance())
                                       .commitNow();
        }
        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        if (!AxMobility.requestUserPermission(this)) {
            axmobilityStart();
        }
    }

    @Override
    protected void onDestroy() {
        AxMobility.close();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        axmobilityStart();
    }

    private void axmobilityStart() {
        /*
         * Here we add callbacks to be called when TR143 events is triggered
         * during the diagnostics.
         */
        final AxEvents events = new AxEvents.Builder()
                                            .withTR143Events(new TR143Events() {
                                                @Override
                                                public void onBegin(TR143Diagnostic tr143Diagnostic) {
                                                    handleOnBegin(tr143Diagnostic);
                                                }

                                                @Override
                                                public void onCompleted(TR143Diagnostic tr143Diagnostic) {
                                                    handleOnCompleted(tr143Diagnostic);
                                                }
                                            })
                                            .build();

        try {
            String clientVersion = getPackageManager().getPackageInfo(getPackageName(), 0)
                                                      .versionName;

            int stringId = getApplicationInfo().labelRes;
            String appName = stringId == 0 ? getApplicationInfo().nonLocalizedLabel.toString()
                                           : getApplicationContext().getString(stringId);

            /*
             * The library is configured to start using the ACS key, some client
             * specific information and the events chosen to be monitored.
             */
            AxSettings settings = AxSettings.Builder()
                                            .withKey(BuildConfig.ACS_KEY)
                                            .withClientName(appName)
                                            .withClientVersion(clientVersion)
                                            .withEvents(events)
                                            .build();

            /* Starts the library */
            AxMobility.start(getApplicationContext(), settings);

            mainViewModel.setCpeid(AxMobility.getCpeID());
            mainViewModel.setAxVersion(AxMobility.version());
        } catch (Exception e) {
            Log.e(Constants.DEFAULT_LOG_TAG, "Client MainActivity axmobilityStart()", e);
        }
    }

    private void handleOnBegin(TR143Diagnostic tr143Diagnostic) {
        String name = "unknown";

        if (tr143Diagnostic instanceof DownloadDiagnostic) {
            name = "Download";
        } else if (tr143Diagnostic instanceof UploadDiagnostic) {
            name = "Upload";
        } else if (tr143Diagnostic instanceof UDPDiagnostic) {
            name = "UDP";
        } else if (tr143Diagnostic instanceof TCPDiagnostic) {
            name = "TCP";
        }

        final String text = "Starting " + name + " diagnostic";

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void handleOnCompleted(TR143Diagnostic tr143Diagnostic) {
        Properties properties =  new Properties();
        double averageSpeed;
        String state;
        String date = SimpleDateFormat.getDateTimeInstance().format(new Date());

        if (tr143Diagnostic instanceof DownloadDiagnostic) {
            DownloadDiagnostic downloadDiagnostic = (DownloadDiagnostic) tr143Diagnostic;
            averageSpeed = downloadDiagnostic.getAverageSpeed();
            state = downloadDiagnostic.getState();
            properties.setProperty(CommonUtils.KEY_DOWNLOAD_SPEED, String.valueOf(averageSpeed));
            properties.setProperty(CommonUtils.KEY_DOWNLOAD_STATE, state);
            properties.setProperty(CommonUtils.KEY_DOWNLOAD_DATE, date);
        } else if (tr143Diagnostic instanceof UploadDiagnostic) {
            UploadDiagnostic uploadDiagnostic = (UploadDiagnostic) tr143Diagnostic;
            averageSpeed = uploadDiagnostic.getAverageSpeed();
            state = uploadDiagnostic.getState();
            properties.setProperty(CommonUtils.KEY_UPLOAD_SPEED, String.valueOf(averageSpeed));
            properties.setProperty(CommonUtils.KEY_UPLOAD_STATE, state);
            properties.setProperty(CommonUtils.KEY_UPLOAD_DATE, date);
        } else if (tr143Diagnostic instanceof UDPDiagnostic) {
            UDPDiagnostic udpDiagnostic = (UDPDiagnostic) tr143Diagnostic;
            averageSpeed = udpDiagnostic.getAverageResponseTime();
            state = udpDiagnostic.getState();
            properties.setProperty(CommonUtils.KEY_UDP_SPEED, String.valueOf(averageSpeed));
            properties.setProperty(CommonUtils.KEY_UDP_STATE, state);
            properties.setProperty(CommonUtils.KEY_UDP_DATE, date);
        } else if (tr143Diagnostic instanceof TCPDiagnostic) {
            TCPDiagnostic tcpDiagnostic = (TCPDiagnostic) tr143Diagnostic;
            averageSpeed = tcpDiagnostic.getAverageResponseTime();
            state = tcpDiagnostic.getState();
            properties.setProperty(CommonUtils.KEY_TCP_SPEED, String.valueOf(averageSpeed));
            properties.setProperty(CommonUtils.KEY_TCP_STATE, state);
            properties.setProperty(CommonUtils.KEY_TCP_DATE, date);
        }

        saveResults(properties);
        mainViewModel.setProperties(properties);
    }

    private void saveResults(Properties properties) {
        String filename = getApplicationContext().getFilesDir().getAbsolutePath() + "/" + CommonUtils.FILE_RESULTS;
        File file = new File(filename);

        try {
            FileWriter writer;

            if (file.exists()) {
                writer = new FileWriter(file, true);
            } else {
                writer = new FileWriter(file);
            }

            properties.store(writer, "TR143 Results");
            writer.close();
        } catch (Exception e) {
            Log.e(Constants.DEFAULT_LOG_TAG, "serialize:", e);
        }
    }
}