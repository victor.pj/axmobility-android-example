package com.example.axmobility.utils;

public class CommonUtils {
    public static final String KEY_DOWNLOAD_SPEED = "averageDownloadSpeed";
    public static final String KEY_DOWNLOAD_STATE = "downloadState";
    public static final String KEY_DOWNLOAD_DATE = "lastDownloadTest";
    public static final String KEY_UPLOAD_SPEED = "averageUploadSpeed";
    public static final String KEY_UPLOAD_STATE = "uploadState";
    public static final String KEY_UPLOAD_DATE = "lastUploadTest";
    public static final String KEY_UDP_SPEED = "averageResponseTime";
    public static final String KEY_UDP_STATE = "UDPState";
    public static final String KEY_UDP_DATE = "lastUDPTest";
    public static final String KEY_TCP_SPEED = "TCPAverageResponseTime";
    public static final String KEY_TCP_STATE = "TCPState";
    public static final String KEY_TCP_DATE = "lastTCPTest";
    public static final String FILE_RESULTS = "results.dat";
}
